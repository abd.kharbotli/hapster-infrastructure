# Terraform Repository

Welcome to the hapster-iac repository! This repository contains Terraform code to manage and provision infrastructure resources in a consistent and automated manner.

## Table of Contents

- [Introduction](#introduction)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Configuration](#configuration)
  - [Usage](#usage)
- [File Structure](#file-structure)

## Introduction

[Terraform](https://www.terraform.io/) is an open-source infrastructure as code (IaC) tool that allows you to define and manage cloud resources in a declarative way. This repository serves as a central place for storing and version-controlling Terraform configurations used to deploy infrastructure on various cloud providers.

## Getting Started

Follow these steps to get started with the Terraform code in this repository.

### Prerequisites

Before using Terraform, ensure you have the following installed:

- [Terraform](https://www.terraform.io/downloads.html)
- Digitalocean credentials
- Git (optional but recommended for version control)

### Installation

1. Clone this repository to your local machine:

   ```
   git clone https://gitlab.hapster.dev/hapster-4.0/hapster-iac.git
   cd hapster-iac
   ```

### Configuration

1. Create the `terraform.tfvars` file and fill in the required variables with your specific values.

2. Review the `variables.tf` file to understand the available variables and their descriptions.

### Usage

1. Initialize Terraform in the project directory:

   ```
   terraform init
   ```

2. Plan the changes to be made:

   ```
   terraform plan
   ```

3. Apply the changes:

   ```
   terraform apply
   ```

   Be cautious as this will create or modify resources on your cloud provider.

## File Structure

The repository is organized as follows:

```
hapster-iac/
  ├── main.tf                # Terraform backend configuration
  ├── ec2.tf                 # Terraform code for managing EC2 instances
  ├── sg.tf                  # Terraform code for managing security groups on AWS
  ├── pvc.tf                 # Terraform code for managing PVC configuration on AWS
  ├── dns.tf                 # Terraform code for managing DNS configurations on Digitalocean
  ├── firewalls.tf           # Terraform code for managing firewalls on Digitalocean
  ├── droplets.tf            # Terraform code for managing droplets
  ├── providers.tf           # Terraform code for managing provider configurations
  ├── variables.tf           # Terraform variables definition
  ├── README.md              # This readme file
  ├── .gitignore             # Git ignore file
  ├── outputs.tf             # Terraform outputs definition
  └── versions.tf            # Terraform version constraints