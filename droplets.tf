resource "digitalocean_droplet" "proxy" {
  name       = "proxy"
  image      = "debian-12-x64"
  region     = var.digitalocean_region
  size       = "s-1vcpu-1gb-intel"
  monitoring = true
  # backups    = true
}

resource "digitalocean_droplet" "k8s" {
  name       = "k8s"
  image      = "debian-12-x64"
  region     = var.digitalocean_region
  size       = "s-4vcpu-8gb-intel"
  monitoring = true
  # backups    = true
}

resource "digitalocean_droplet" "k8s-worker01" {
  name       = "k8s-worker01"
  image      = "debian-12-x64"
  region     = var.digitalocean_region
  size       = "s-4vcpu-16gb-amd"
  monitoring = true
  # backups    = true
}

resource "digitalocean_droplet" "jenkins-build" {
  name       = "jenkins-build"
  image      = "debian-12-x64"
  region     = var.digitalocean_region
  size       = "s-2vcpu-4gb-120gb-intel"
  monitoring = true
  # backups    = true
}

resource "digitalocean_droplet" "jenkins-sonarqube" {
  name       = "jenkins-sonarqube"
  image      = "debian-12-x64"
  region     = var.digitalocean_region
  size       = "s-4vcpu-8gb-intel"
  monitoring = true
  # backups    = true
}

resource "digitalocean_droplet" "hadoop" {
  name       = "hadoop"
  image      = "debian-12-x64"
  region     = var.digitalocean_region
  size       = "s-2vcpu-4gb-120gb-intel"
  monitoring = true
  # backups    = true
}

# resource "digitalocean_droplet" "kafka" {
#   name       = "kafka"
#   image      = "debian-12-x64"
#   region     = var.digitalocean_region
#   size       = "s-2vcpu-4gb-intel"
#   monitoring = true
#   backups    = true
# }

# resource "digitalocean_droplet" "testing" {
#   name       = "testing"
#   image      = "debian-12-x64"
#   region     = var.digitalocean_region
#   size       = "s-4vcpu-8gb-intel"
#   monitoring = true
#   backups    = true
# }

resource "digitalocean_droplet" "nfs" {
  name       = "nfs"
  image      = "debian-12-x64"
  region     = var.digitalocean_region
  size       = "s-1vcpu-2gb-70gb-intel"
  monitoring = true
  # backups    = true
}
