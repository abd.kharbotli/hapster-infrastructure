
locals {
  rdp_ports = ["3389", "44945"]
}
resource "aws_security_group" "rdp" {
  name        = "rdp"
  description = "Allow RDP traffic"
  vpc_id      = data.aws_vpc.main.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "rdp-sg"
  }
}

resource "aws_security_group_rule" "rdp_ingress" {
  count             = length(local.rdp_ports)
  type              = "ingress"
  from_port         = local.rdp_ports[count.index]
  to_port           = local.rdp_ports[count.index]
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.rdp.id
}
