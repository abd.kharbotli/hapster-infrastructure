# data "aws_ami" "ws-2019-full" {
#   most_recent = true
#   owners      = ["amazon"]

#   filter {
#     name   = "name"
#     values = ["EC2LaunchV2-Windows_Server-2019-English-Full-Base-*"]
   
#     # name   = "image-id"
#     # values = ["ami-000000000000"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }
# }

resource "tls_private_key" "hapster" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "hapster" {
  key_name   = "hapster"
  public_key = tls_private_key.hapster.public_key_openssh
}


resource "aws_instance" "jenkins-build-windows" {
  
  ami                    = "ami-0cbfb356719ec0a32"
  instance_type          = "t3.medium"
  key_name               = "hapster"
  subnet_id              = [for subnet in data.aws_subnet.all : subnet.id if length(subnet.tags) == 0][0]
  vpc_security_group_ids = [aws_security_group.rdp.id]
  depends_on             = [aws_key_pair.hapster]

  root_block_device {
    volume_size = 60
  }
  disable_api_termination = true

  tags = {
    Name = "jenkins-build-windows-ec2"
  }
}

resource "aws_eip" "jenkins-build-windows" {
  instance = aws_instance.jenkins-build-windows.id
  vpc      = true
  tags = {
    Name = "jenkins-build-windows-eip"
  }
}
