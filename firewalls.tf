locals {
  ssh_ports                   = ["22", "53974"]
  web_ports                   = ["80", "443"]
  sonarqube-jenkins_web_ports = ["8080", "9000"]
  hadoop_ports                = ["9864", "9870"]
  # testing_ports               = ["80", "8090", "8091" , "8089" , "8088"]
  # k8s_ports                   = ["2379-2380", "6443", "10250", "10257", "10259"]
  # k8s-worker01_ports          = ["10250"]
  k8s_ports                   = ["1-65535"]
  k8s-worker01_ports          = ["1-65535"]
  proxy_private_ip              = digitalocean_droplet.proxy.ipv4_address_private
  k8s_private_ip              = digitalocean_droplet.k8s.ipv4_address_private
  k8s-worker01_private_ip     = digitalocean_droplet.k8s-worker01.ipv4_address_private
}

resource "digitalocean_firewall" "ssh" {
  name = "ssh"

  droplet_ids = [digitalocean_droplet.jenkins-build.id, digitalocean_droplet.jenkins-sonarqube.id, 
  digitalocean_droplet.hadoop.id, digitalocean_droplet.k8s.id, digitalocean_droplet.nfs.id, 
  digitalocean_droplet.k8s-worker01.id, digitalocean_droplet.proxy.id]
  # digitalocean_droplet.kafka.id, digitalocean_droplet.testing.id

  dynamic "inbound_rule" {
    for_each         = local.ssh_ports
    content {
      protocol         = "tcp"
      port_range       = inbound_rule.value
      source_addresses = ["0.0.0.0/0"]
    }
  }
}

resource "digitalocean_firewall" "web" {
  name = "web"
  
  droplet_ids = [digitalocean_droplet.proxy.id]

  dynamic "inbound_rule" {
    for_each = local.web_ports
    content {
      protocol         = "tcp"
      port_range       = inbound_rule.value
      source_addresses = ["0.0.0.0/0"]
    }
  }
}

resource "digitalocean_firewall" "sonarqube-jenkins" {
  name = "sonarqube-jenkins"

  droplet_ids = [digitalocean_droplet.jenkins-sonarqube.id]

  dynamic "inbound_rule" {
    for_each = local.sonarqube-jenkins_web_ports
    content {
      protocol         = "tcp"
      port_range       = inbound_rule.value
      source_addresses = [local.proxy_private_ip]
      }
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "61847"
    source_addresses = ["0.0.0.0/0"]
  }
}

resource "digitalocean_firewall" "hadoop" {
  name = "hadoop"
  
  droplet_ids = [digitalocean_droplet.hadoop.id]

  dynamic "inbound_rule" {
    for_each = local.hadoop_ports
    content {
      protocol         = "tcp"
      port_range       = inbound_rule.value
      source_addresses = [local.proxy_private_ip]
    }
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "9864"
    source_addresses = ["0.0.0.0/0"]
  }
}

resource "digitalocean_firewall" "k8s" {
  name = "k8s"
  
  droplet_ids = [digitalocean_droplet.k8s.id]

  dynamic "inbound_rule" {
    for_each = local.k8s_ports 
    content {
      protocol         = "tcp"
      port_range       = inbound_rule.value
      source_addresses = [local.k8s-worker01_private_ip]
    }
  }  
}

resource "digitalocean_firewall" "k8s-worker01" {
  name = "k8s-worker01"
  
  droplet_ids = [digitalocean_droplet.k8s-worker01.id]

  dynamic "inbound_rule" {
    for_each = local.k8s-worker01_ports 
    content {
      protocol         = "tcp"
      port_range       = inbound_rule.value
      source_addresses = [local.k8s_private_ip]
    }
  }
 
  inbound_rule {
    protocol         = "tcp"
    port_range       = "30000-32767"
    source_addresses = [local.proxy_private_ip]
  }
}

resource "digitalocean_firewall" "nfs" {
  name = "nfs"
  
  droplet_ids = [digitalocean_droplet.nfs.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "2049"
    source_addresses = [local.k8s_private_ip, local.k8s-worker01_private_ip]
  }    
}

# resource "digitalocean_firewall" "testing" {
#   name = "testing"
  
#   droplet_ids = [digitalocean_droplet.testing.id]

#   dynamic "inbound_rule" {
#     for_each = local.testing_ports
#     content {
#       protocol         = "tcp"
#       port_range       = inbound_rule.value
#       source_addresses = [local.proxy_private_ip]
#     }
#   }
# }

# resource "digitalocean_firewall" "kafka" {
#   name = "kafka"
  
#   droplet_ids = [digitalocean_droplet.kafka.id]

#     inbound_rule {
#       protocol         = "tcp"
#       port_range       = "9092"
#       source_addresses = ["0.0.0.0/0"]
#     }
# }

resource "digitalocean_firewall" "outbound" {
  name = "outbound"

  droplet_ids = [digitalocean_droplet.jenkins-build.id, digitalocean_droplet.jenkins-sonarqube.id, 
  digitalocean_droplet.hadoop.id, digitalocean_droplet.k8s.id, digitalocean_droplet.nfs.id, 
  digitalocean_droplet.k8s-worker01.id, digitalocean_droplet.proxy.id]
  # digitalocean_droplet.kafka.id, digitalocean_droplet.testing.id

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0"]
  }
}
