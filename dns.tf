locals {
  proxy_public_ip_address = digitalocean_droplet.proxy.ipv4_address
  sub_domains             =  ["admin", "api", "kc", "admin-dev", "kc-dev", "api-dev", "sq", "swagger", 
  "jenkins", "lrs", "hadoop-name", "tts", "k8s-dash"]
}

resource "digitalocean_domain" "default" {
  name = var.staging_domain
}

resource "digitalocean_record" "sub_domain" {
  for_each = toset(local.sub_domains)

  domain = digitalocean_domain.default.id
  type   = "A"
  name   = each.key
  value  = local.proxy_public_ip_address
}