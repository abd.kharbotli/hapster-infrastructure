variable "domain" {
  default = "domain.com"
}
variable "staging_domain" {
  default = "staging-hsp.com"
}

variable "digitalocean_token" {}


variable "digitalocean_region" {
  default = "fra1" //Frankfurt, Germany
}

# AWS Settings
variable "aws_access_key" {}

variable "aws_secret_key" {}

variable "aws_region" {
  default = "us-east-1"
}
