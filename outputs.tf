
output "droplets" {
  value = {
    proxy_public_ip             = digitalocean_droplet.proxy.ipv4_address
    jenkins_build_public_ip     = digitalocean_droplet.jenkins-build.ipv4_address
    jenkins_sonarqube_public_ip = digitalocean_droplet.jenkins-sonarqube.ipv4_address
    hadoop_public_public_ip     = digitalocean_droplet.hadoop.ipv4_address
    # kafka_public_public_ip      = digitalocean_droplet.kafka.ipv4_address
    # testing_public_public_ip    = digitalocean_droplet.testing.ipv4_address
    k8s_public_public_ip        = digitalocean_droplet.k8s.ipv4_address
    nfs_public_public_ip        = digitalocean_droplet.nfs.ipv4_address
    k8s-worker01_public_ip      = digitalocean_droplet.k8s-worker01.ipv4_address
  }
}

output "ec2_instances" {
  value = {
    jenkins_build_windows_public_ip       = aws_eip.jenkins-build-windows.public_ip
    #jenkins_build_windows_key      = nonsensitive(tls_private_key.hapster.private_key_pem)

  }
}