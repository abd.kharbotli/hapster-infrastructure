terraform {
  cloud {
    organization = "algolmir"

    workspaces {
      name = "hapster"
    }
  }
}