terraform {
  backend "http" {
    config = {
        address = var.gitlab_address
        username = var.gitlab_username
        password = var.gitlab_access_token
    }
  }
}